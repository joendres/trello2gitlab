# Contributing

## References

* [Trello API](https://developer.atlassian.com/cloud/trello/)
* [Gitlab Issues API](https://docs.gitlab.com/ee/api/issues.html)
* [Create one issue](https://docs.gitlab.com/ee/api/issues.html#new-issue)
