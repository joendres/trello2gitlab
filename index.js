const fs = require("fs");

const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const argv = yargs(hideBin(process.argv)).argv;

const config = { files: [], users: {}, labels: {}, lists: {}, project_id: 0, access_token: "", };

//#region MAIN
config.files.concat(argv._)
    .filter((elem, pos, arr) => arr.indexOf(elem) === pos)
    .forEach(fname => handle(fname));

function handle(fname) {
    const cont = JSON.parse(fs.readFileSync(fname));
    if (argv.generateFrom) {
        if (!config.files.includes(fname)) {
            config.files.push(fname);
        }
        generate(cont);
    } else {
        transfer(cont);
    }
}
//#endregion

if (argv.generateFrom) {
    console.log(JSON.stringify(config, null, 2));
}

function transfer(obj) {
    obj.cards.forEach(card => {
        sendIssue(makeIssue(card));
    });

    function makeIssue(card) {
        const issue = { title: card.name, };
        // Add labels
        // TODO Lookup Label name and translate via config
        let labels = card.labels.map(l => l.name);

        // Set label that corresponds with list name
        // TODO Lookup gitlab labels
        // TODO break if falsy label
        // TODO handle "Closed"
        labels.push(obj.lists.find(l => l.id === card.idList).name);
        issue.labels = labels.filter(e => !!e).join(",");

        // TODO add shortened description

        // Add due date if it is set
        if (card.due) {
            issue.due_date = card.due.slice(0, 10);
        }

        return issue;
    }

    function sendIssue(issue) {
        // TODO Rather send a json body, 
        const url = new URL(`https://glitlab.com/api/v4/projects/${config.project_id}/issues`);
        // url.search = (new URLSearchParams(issue)).toString();

        const fetchInfo = {
            method: "POST",
            headers: {
                ["Content-Type"]: "application/json",
                Authorization: `Bearer ${config.access_token}`,
            },
            body: JSON.stringify(issue),
        };
        console.log(url.href);
    }

}

function generate(obj) {
    obj.members.forEach(m => {
        config.users[m.fullName] = "";
    });
    for (const key in obj.labelNames) {
        if (obj.labelNames[key]) {
            config.labels[obj.labelNames[key]] = obj.labelNames[key];
        }
    }
    obj.lists.forEach(l => {
        config.lists[l.name] = l.name;
    });
}
